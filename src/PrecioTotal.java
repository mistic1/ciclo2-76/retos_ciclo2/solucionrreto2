// Inicio de la solución
public class PrecioTotal {
    private double totalPrecios;
    private double totalNacional; 
    private double totalUrbano; 
    private Flete[] flete;

    public PrecioTotal(Flete[] flete){
        this.flete = flete; 
    }

    public void calcularTotales(){

        for (int i = 0; i <= flete.length -1; i++) {
            
            totalPrecios += flete[i].calcularPrecio();

            if (flete[i].getClass() == Nacional.class) {
                totalNacional += flete[i].calcularPrecio();
            } else {
                totalUrbano += flete[i].calcularPrecio();
            }

        }
    }

    public void mostrarTotales(){
        calcularTotales();
        System.out.println("Total Fletes " + totalPrecios);
        System.out.println("Total Nacional " + totalNacional);
        System.out.println("Total Urbano " + totalUrbano);
    }
    
}
