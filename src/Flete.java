public class Flete {
    // Constantes
    private static final double PESO = 10.0;
    private static final double TAMANIO = 4.5;
    private static final double PRECIO_BASE = 1000.0;
    // Atributos
    private double peso;
    private double tamanio;
    private double precioBase;
    // Constructores
    public Flete (double peso, double tamanio){
        this.peso = peso;
        this.tamanio = tamanio;
        this.precioBase = PRECIO_BASE;
        }
    public Flete (double precioBase){
        this.precioBase = precioBase;
        this.peso = PESO;
        this.tamanio = TAMANIO;
    }
    public Flete() {
        this.peso = this.PESO;
        this.tamanio = this.TAMANIO;
        this.precioBase = this.PRECIO_BASE;
    }
    // Modificadores
    public double getPeso() {
        return peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    public double getTamanio() {
        return tamanio;
    }
    public void setTamanio(double tamanio) {
        this.tamanio = tamanio;
    }
    public double getPrecioBase() {
        return precioBase;
    }
    public void setPrecioBase(double precioBase) {
        this.precioBase = precioBase;
    }
    // Metodos
    public double calcularPrecio(){
        return 0.0;
    }
}
