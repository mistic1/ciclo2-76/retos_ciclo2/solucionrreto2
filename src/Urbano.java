public class Urbano extends Flete{
    // Constantes
    private final static int TIEMPO = 2;
    // Constructores
    public Urbano(double peso, double tamanio){
        super(peso, tamanio);
    }

    public Urbano(double precioBase){
        super (precioBase);
    }

    public Urbano(){
        super ();
    }
    // Metodos
    @Override
    public double calcularPrecio(){
    // Calculos
        double precioFinal = super.getPrecioBase() + (super.getPeso() * super.getTamanio() * TIEMPO); 
        return precioFinal;
    }
}
