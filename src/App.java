public class App {
    public static void main(String[] args) throws Exception {
        
        Flete flete[] = new Flete[5];
        flete[0] = new Nacional(100.0, 10.0);
        flete[1] = new Nacional(200);
        flete[2] = new Urbano(150, 20.0);
        flete[3] = new Urbano();
        flete[4] = new Nacional();
        PrecioTotal solucion = new PrecioTotal(flete);
        solucion.mostrarTotales();

    }
}
