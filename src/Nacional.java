public class Nacional extends Flete{
    // Constantes
    private static final double CAPACIDAD = 8.0;

    // Constructores
    public Nacional(double peso, double tamanio){
        super(peso, tamanio);
        }
    public Nacional(double precioBase){
        super(precioBase);
    }
    public Nacional() {
        super();
    }
    // Metodos
    @Override
    public double calcularPrecio(){
        double precioFinal = super.getPrecioBase() + (super.getPeso() * super.getTamanio() * CAPACIDAD); 
        return precioFinal;
    }
}
